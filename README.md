# README for aoe2de-seteffects
This project is a direct translation of certain tasks from Greg Stein's [SenseiDE](https://github.com/gregstein/SenseiDE) that the author wanted to replicate on a GNU/Linux+Steam+Proton+AoE2DE installation.

## Upstream
This is the original work for this type of activity, [https://gitlab.com/bgstack15/aoe2de-seteffects](https://gitlab.com/bgstack15/aoe2de-seteffects).

## Alternatives
If you are running the game on Windows, then go use [SenseiDE](https://github.com/gregstein/SenseiDE) directly. Its source is available if you want to learn exactly what it is doing and replicate it manually.

## Dependencies
* 7za (unzip would suffice, but 7za is currently hardcoded)

## References
[SenseiDE](https://github.com/gregstein/SenseiDE)

## Differences from upstream
N/A
