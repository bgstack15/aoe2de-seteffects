#!/bin/sh
# Filename: set-effects.sh
# Locations: https://gitlab.com/bgstack15/aoe2de-seteffects
#            /mnt/public/Support/Games/aoe2de-on-linux/seteffects/
# Author: bgstack15@gmail.com
# Startdate: 2021-04-08
# SPDX-License-Identifier: CC-BY-SA-4.0
# Title: Set Effects for AoE2DE on Linux
# Purpose: Make it easy to enable or disable the specific effects in-game and in-menu
# Usage:
#    ./set-effects.sh -i # to initialize
#    ./set-effects.sh trim # to select trimmed effects
#    ./set-effects.sh orig # to select original, full effects list
# Reference:
#    https://github.com/gregstein/SenseiDE
#       specifically its MO.zip and DF.zip files.
#       and the logic for manipulating these zip files and also the atlases/*json files
# Improve:
# Documentation:

test -z "${SE_COMMON_DIR}" && SE_COMMON_DIR=~/.local/share/Steam/steamapps/common/AoE2DE/resources/_common
SE_WPFG_DIR="${SE_COMMON_DIR}/wpfg"
SE_WPFG_ORIG_DIR="${SE_COMMON_DIR}/wpfg-orig"
SE_WPFG_DF_DIR="${SE_COMMON_DIR}/wpfg-df"
SE_WPFG_MO_DIR="${SE_COMMON_DIR}/wpfg-mo"
SE_DF_ZIP="/mnt/public/Support/Games/aoe2de-on-linux/seteffects/DF.zip" # DF_ZIP is the bog-standard menu objects
SE_MO_ZIP="/mnt/public/Support/Games/aoe2de-on-linux/seteffects/MO.zip" # MO_ZIP is the stripped-down menu objects that are the value-add from SenseiDE
SE_STATUS_FILE="${SE_COMMON_DIR}/.set-effects.txt"
SE_ATLASES_DIR="${SE_COMMON_DIR}/particles/textures/atlases"

init() {
   echo "Initializing."
   # validate all inputs first
   if test ! -r "${SE_DF_ZIP}" || test ! -r "${SE_MO_ZIP}" || test ! -d "${SE_WPFG_DIR}" || \
      test ! -d "${SE_ATLASES_DIR}" ;
   then
      {
         echo "Not all dependencies are met. Ensure:"
         echo "${SE_DF_ZIP} is a zip."
         echo "${SE_MO_ZIP} is a zip."
         echo "${SE_WPFG_DIR} exists as a dir."
         echo "${SE_ATLASES_DIR} exists as a dir."
      } 1>&2
      exit 1
   fi
   
   # backup orig dir
   if test -d "${SE_WPFG_ORIG_DIR}" ;
   then
      # we are initializing, but a backup dir already exists...
      export SE_WPFG_ORIG_DIR="${SE_WPFG_ORIG_DIR}.$( date "+%F" )"
      echo "Using a backup dirname of ${SE_WPFG_ORIG_DIR}"
   fi

   # starting fresh. Let's back up the orig files.
   cd "$( dirname "${SE_WPFG_ORIG_DIR}" )" ; cp -pr "${SE_WPFG_DIR}" "$( basename "${SE_WPFG_ORIG_DIR}" )"

   cd "$( dirname "${SE_ATLASES_DIR}" )" ; cp -pr "$( basename "${SE_ATLASES_DIR}" )" "$( basename "${SE_ATLASES_DIR}" ).$( date "+%F" )"

   # explode MO zip
   mkdir -p "${SE_WPFG_MO_DIR}" 
   cd "${SE_WPFG_MO_DIR}" ; 7za -aoa x "${SE_MO_ZIP}"

   # explode DF zip, and fix filenames
   mkdir -p "${SE_WPFG_DF_DIR}" 
   cd "${SE_WPFG_DF_DIR}" ; 7za -aoa x "${SE_DF_ZIP}"
   cd dialog ; for word in * ; do newword="$( echo "${word}" | sed -r -e 's/^dialogDialog/Dialog/;' -e 's/^dialogdialog/dialog/;' )" ; test "${newword}" != "${word}" && mv "${word}" "${newword}" ; done

   # touch status file
   date -u "+%FT%TZ" > "${SE_STATUS_FILE}"

   echo "Done with initialization."
}

switch_to() {
   # call: switch_to trim
   #     or switch_to orig
   ___st_target="${1}"
   case "${___st_target}" in

      "trim"|"MO")
         # the good stuff
         cd "${SE_WPFG_DIR}" 
         for word in screen dialog ;
         do
            cd "${word}" ; cp -pr "${SE_WPFG_MO_DIR}/${word}"/* .
            cd ..
         done
         # disable in-game effects
         cd "${SE_ATLASES_DIR}" ; mkdir -p off
         mv -f *.json off/
         cd off
         # hardcoded effects I want to keep.
         mv -i villager_* trails* upgrade* water_des* laser* fire* explos* unit_dust* snow_* \
            impact_petard* impact_explo* impact_treb* research* spawn* \
            ..
         ;;

      "orig"|"DF")
         # original menu assets
         cd "${SE_WPFG_DIR}" 
         for word in screen dialog ;
         do
            cd "${word}" ; cp -pr "${SE_WPFG_DF_DIR}/${word}"/* .
            cd ..
         done

         # re-enable all in-game effects
         cd "${SE_ATLASES_DIR}"
         if ! test -d "off" ;
         then
            echo "No in-game effects to re-enable."
         else
            mv -f off/*.json .
            rmdir off
         fi
         ;;

      *)
         echo "Choose orig or trim, to set the menu effects and some in-game effects for AoE2DE."
         #echo "Option ${___st_target} not supported yet. Aborted." 1>&2 ; exit 1
         ;;

   esac
}

# MAIN

# initialize if necessary
if test ! -f "${SE_STATUS_FILE}" || echo " ${@} " | grep -qE -e ' -i\>| --init(ialize)?\>' ;
then
   init
fi

switch_to "${1}"
